package ru.t1.aksenova.tm.controller;

import ru.t1.aksenova.tm.api.IProjectController;
import ru.t1.aksenova.tm.api.IProjectService;
import ru.t1.aksenova.tm.model.Project;
import ru.t1.aksenova.tm.util.TerminalUtil;

import java.util.List;

public final class ProjectController implements IProjectController {

    public final IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void createProject() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("[ENTER PROJECT NAME:]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER PROJECT DESCRIPTION:]");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.create(name, description);
        if (project == null) System.err.println("[ERROR PROJECT]");
        else System.out.println("[OK]");
    }

    @Override
    public void showProjects() {
        System.out.println("[PROJECT LIST]");
        int index = 1;
        final List<Project> projects = projectService.findAll();
        for (final Project project: projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project.getName());
            System.out.println("Description: " + project.getDescription());
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearProjects() {
        System.out.println("[PROJECTS CLEAR]");
        projectService.clear();
        System.out.println("[OK]");
    }

}
