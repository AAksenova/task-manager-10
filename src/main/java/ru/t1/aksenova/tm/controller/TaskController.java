package ru.t1.aksenova.tm.controller;

import ru.t1.aksenova.tm.api.ITaskController;
import ru.t1.aksenova.tm.api.ITaskService;
import ru.t1.aksenova.tm.model.Task;
import ru.t1.aksenova.tm.util.TerminalUtil;

import java.util.List;

public final class TaskController implements ITaskController {

    public final ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void createTask() {
        System.out.println("[TASK CREATE]");
        System.out.println("[ENTER TASK NAME:]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER TASK DESCRIPTION:]");
        final String description = TerminalUtil.nextLine();
        final Task task= taskService.create(name, description);
        if (task == null) System.err.println("[ERROR TASK]");
        else System.out.println("[OK]");
    }

    @Override
    public void showTasks() {
        System.out.println("[TASK LIST]");
        int index = 1;
        final List<Task> tasks = taskService.findAll();
        for (final Task task: tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task.getName());
            System.out.println("Description: " + task.getDescription());
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[TASK CLEAR]");
        taskService.clear();
        System.out.println("[OK]");
    }

}
