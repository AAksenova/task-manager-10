package ru.t1.aksenova.tm.api;

public interface ITaskController {

    void createTask();

    void showTasks();

    void clearTasks();

}
