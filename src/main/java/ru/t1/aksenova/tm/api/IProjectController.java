package ru.t1.aksenova.tm.api;

public interface IProjectController {

    void createProject();

    void showProjects();

    void clearProjects();

}
