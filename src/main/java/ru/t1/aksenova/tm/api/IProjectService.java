package ru.t1.aksenova.tm.api;

import ru.t1.aksenova.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project create(String name, String description);

}
